# stand-up_LMStudio_on_Linux-Mint-20

#### Let's take [LMStudio]((lmstudio.ai)) out for a test drive on Linux Mint 20...

Inspired by [this charmingly odd video](https://www.youtube.com/watch?v=GOjuuw8AvBc&list=PLp9pLaqAQbY3LbMiQqQ0iwv1W6J10WzZS&index=79) by Matthew Berman:

![secure-your-AI](/uploads/7f634000d5f9514b89781d74fd098308/secure-your-AI.png)

We will work through a series of _"tag along, kiddo"_ mini-projects...

## projects: Get __**"up & going"**__ ASAP w/ **Standalone Local LLMs** w/ [Matthew Berman](https://www.youtube.com/playlist?list=PLp9pLaqAQbY3LbMiQqQ0iwv1W6J10WzZS)

### develop: Standalone Local Models using "Prepper Strategy" (from [Matthew Berman](https://www.youtube.com/playlist?list=PLp9pLaqAQbY3LbMiQqQ0iwv1W6J10WzZS)) 
* watch (~13m): [Store ALL Of Humanity's Knowledge IN YOUR HOME (Tutorial)](https://www.youtube.com/watch?v=GOjuuw8AvBc&list=PLp9pLaqAQbY3LbMiQqQ0iwv1W6J10WzZS&index=78&pp=iAQB)
  * high-value timestamps below...
    * ts 3m57s: Latest LLMs can easily fit on a Portable SSD Drive
      * You need to store both the LLM(s) and the dev environments.  
      * Consider physical protection (against Fire, EMP, moisture, etc.) as well.
    * ts [4m15s](https://youtu.be/GOjuuw8AvBc?t=4m15s): Choose from Latest, Largest, Quantized, and Uncensored LLMs
      * ...quantization trades off _**some**_ performance for the _**great**_ ability to run on commodity HW
    * ts [5m35s](https://youtu.be/GOjuuw8AvBc?t=5m35s) Run you LLM in a Local IDE
      * such as: LMStudio, Ollama. TextGen WebUI, etc.
        * LMStudio: Super-friendly GUI, so recommended for beginners;
        * Ollama: No GUI, but very useful for the power of the raw interface;
          * It does take a little more technical know-how to get it working
        * TextGen WebUI: Has a GUI, not quite as beginner-friendly, but well documented & approachable.
    * ts 6m20s: Optical Storage (Blue Ray) is another option
      * ...to offset the risk from limited lifetime (~5yrs?) of SSDs
    * ts [8m30s](https://youtu.be/GOjuuw8AvBc?t=8m30s): **LLM Installation Walk-Through**
    * ts [9m28s](https://youtu.be/GOjuuw8AvBc?t=8m30s): Sizing the "latest & greatest" LLM choices for **your** Localhost
    * ts 9m49s: `mixtral dolphin` is a good fine-tuned (and __**uncensored**__) choice
* watch (~12m): [Run ANY Open-Source Model LOCALLY (LM Studio Tutorial)](https://www.youtube.com/watch?v=yBI1nPep72Q&list=PLp9pLaqAQbY3LbMiQqQ0iwv1W6J10WzZS&index=64&t=624s&pp=iAQB) 
  * **NB: You can make calls to your local LLM model the same way you would use OpenAI's API**
* watch (~32m): [MemGPT 🧠 Giving AI Unlimited Prompt Size (Big Step Towards AGI?)](https://www.youtube.com/watch?v=QQ2QOPWZKVc)

---

### Caveat: LMStudio is _**not**_ open source.

#### discussion (HackerNews): [LM Studio – Discover, download, and run local LLMs](https://news.ycombinator.com/item?id=38377072) 

> advice ([HN comment](https://news.ycombinator.com/item?id=38378543)): If you're looking to do the same with open source code, you could likely run [Ollama](https://github.com/jmorganca/ollama) and [a UI](https://github.com/ollama-webui/ollama-webui ).


